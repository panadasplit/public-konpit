import { push } from "connected-react-router";

import { SIGNUP_REQUEST, SIGNUP_FAIL, SIGNUP_SUCCESS } from "./types";

// SIGNUP USER
export const register = (email, password) => (dispatch, getState, getFirebase) => {
  dispatch({
    type: SIGNUP_REQUEST
  });


  getFirebase().auth().createUserWithEmailAndPassword(email, password)
    .then(res => {
      dispatch({
        type: SIGNUP_SUCCESS,
        payload: res.data
      });
      dispatch(push("/login"));
    })
    .catch(err => {
      dispatch({
        type: SIGNUP_FAIL,
        payload: err.data
      });
    });
};
