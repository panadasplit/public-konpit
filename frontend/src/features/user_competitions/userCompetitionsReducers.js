import {
  GET_USER_COMPETIONS_REQUEST,
  GET_USER_COMPETIONS_SUCCESS,
  GET_USER_COMPETIONS_FAIL,
  GET_COMPETITION_REQUEST,
  GET_COMPETITION_SUCCESS,
  GET_COMPETITION_FAIL,
  CREATE_NEW_COMPETITION_REQUEST,
  CREATE_NEW_COMPETITION_SUCCESS,
  CREATE_NEW_COMPETITION_FAIL,
  UPDATE_COMPETITION_REQUEST,
  UPDATE_COMPETITION_SUCCESS,
  UPDATE_COMPETITION_FAIL,
  DELETE_COMPETITION_REQUEST,
  DELETE_COMPETITION_SUCCESS,
  DELETE_COMPETITION_FAIL
} from "./types";

const initialState = {
  competitions: [],
  competition: {
    id: 0,
    name: "",
    description: "",
    slug: ""
  },
  isLoading: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_USER_COMPETIONS_REQUEST:
      return { ...state, isLoading: true };
    case GET_USER_COMPETIONS_SUCCESS:
      return { ...state, isLoading: false, competitions: action.competitions };
    case GET_USER_COMPETIONS_FAIL:
      return { ...state, isLoading: false, competitions: [] };
    case GET_COMPETITION_REQUEST:
      return { ...state, isLoading: true };
    case GET_COMPETITION_SUCCESS:
      return { ...state, isLoading: false, competition: action.competition };
    case GET_COMPETITION_FAIL:
      return { ...state, isLoading: false };
    case CREATE_NEW_COMPETITION_REQUEST:
      return { ...state, isLoading: true };
    case CREATE_NEW_COMPETITION_SUCCESS:
      return { ...state, isLoading: false };
    case CREATE_NEW_COMPETITION_FAIL:
      return { ...state, isLoading: false };
    case UPDATE_COMPETITION_REQUEST:
      return { ...state, isLoading: true };
    case UPDATE_COMPETITION_SUCCESS:
      return { ...state, isLoading: false, competition: action.competition };
    case UPDATE_COMPETITION_FAIL:
      return { ...state, isLoading: false };
    case DELETE_COMPETITION_REQUEST:
      return { ...state, isLoading: true };
    case DELETE_COMPETITION_SUCCESS:
      const new_competitions = state.competitions.filter(
        competition => competition.id !== action.id_competition
      );
      return { ...state, isLoading: false, competitions: new_competitions };
    case DELETE_COMPETITION_FAIL:
      return { ...state, isLoading: false };
    default:
      return state;
  }
}
