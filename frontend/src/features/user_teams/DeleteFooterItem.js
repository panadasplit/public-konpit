import React, { Component } from "react";

import { connect } from "react-redux";

import { deleteTeam } from "./actions";

class DeleteFooterItem extends Component {
  onClick = e => {
    e.preventDefault();
    this.props.deleteTeam(this.props.id_competition, this.props.team.id);
  };

  render() {
    return (
      <button
        className="button is-medium is-light card-footer-item"
        onClick={this.onClick}
      >
        {" "}
        Delete
      </button>
    );
  }
}

export default connect(
  null,
  { deleteTeam }
)(DeleteFooterItem);
