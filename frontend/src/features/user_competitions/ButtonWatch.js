import React from "react";
import { Link } from "react-router-dom";

const ButtonWatch = slug => {
  return (
    <div class="buttons is-right">
      <Link to={`/competition/${slug}`} className="button is-primary is-right">
        Watch
      </Link>
    </div>
  );
};

export default ButtonWatch;
