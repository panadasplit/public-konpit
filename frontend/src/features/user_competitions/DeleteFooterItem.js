import React, { Component } from "react";

import { connect } from "react-redux";

import { deleteCompetition } from "./actions";

class DeleteFooterItem extends Component {
  onClick = e => {
    e.preventDefault();
    this.props.deleteCompetition(this.props.competition.id);
  };

  render() {
    return (
      <button
        className="button is-medium is-danger is-light card-footer-item"
        onClick={this.onClick}
      >
        {" "}
        Delete
      </button>
    );
  }
}

export default connect(
  null,
  { deleteCompetition }
)(DeleteFooterItem);
