""" League Models """

from typing import List
from pydantic import BaseModel

from src.core.database import DB

from .match import Match


class LeagueKey(BaseModel):
    """ represent ids of a league """
    id: str
    id_competition: str = None

    @property
    def reference(self):
        """ return league document reference """
        return DB().client.collection("leagues").document(self.id)

    @property
    def document(self):
        """ return league document """
        return self.reference.get()


class League(LeagueKey):
    """ represent a league """

    matchs: List[Match] = []


class LeagueOut(League):
    """ represent a league in htpp response """
    pass
