""" User models """

from pydantic import BaseModel

from src.crud.user import get_user_reference


class User(BaseModel):

    uid: str

    @property
    def reference(self):
        """ return user firestore document reference """
        return get_user_reference(self.uid)
