import React from "react";

function CompetitionHeader(props) {
  const { competition } = props;
  return (
    <section className="hero is-primary">
      <div className="hero-body">
        <div className="container has-text-centered">
          <h1 className="title">{competition.name}</h1>
          <h2 className="subtitle">{competition.description}</h2>
        </div>
      </div>
    </section>
  );
}

export default CompetitionHeader;
