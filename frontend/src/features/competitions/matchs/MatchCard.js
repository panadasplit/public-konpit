import React, { Component } from "react";

import PropTypes from "prop-types";

export default class UserMatchCard extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired
  };

  render() {
    const { match } = this.props;
    return (
      <div className="column is-half is-offset-one-quarter">
        <div className="box">
          <div className="card">
            <div className="card-content">
              <div className="content">
                <div className="columns is-vcentered is-capitalized has-text-weight-bold">
                  <div className="column is-two-fifths">
                    <p className="has-text-left ">{match.home_team.name}</p>
                  </div>
                  <div className="column">
                    <p className="has-text-centered is-size-4	">
                      {match.home_score} - {match.away_score}
                    </p>
                  </div>
                  <div className="column is-two-fifths">
                    <p className="has-text-right is-capitalized">
                      {match.away_team.name}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
