""" Competition routers """

from datetime import datetime

from slugify import slugify

from fastapi import APIRouter, Depends

from src.core.authentication import get_current_user_authorizer
from src.core.validators import is_competition_owner

from src.crud.competition import (
    add_competition,
    get_competition,
    get_competition_by_slug,
    get_user_competitions,
    update_competition,
    delete_competition,
)

from src.models.user import User
from src.models.competition import CompetitionKey, CompetitionIn, CompetitionOut, CompetitionInDb

router = APIRouter()


@router.post("", status_code=201)
def create_competition(competition: CompetitionIn,
                       user: User = Depends(get_current_user_authorizer())):
    """ create and return a new competition """

    # generate unique slug
    slug = get_unique_slug(competition.name)
    date_creation = datetime.now()

    new_competition = add_competition(
        CompetitionInDb(**competition.dict(),
                        slug=slug,
                        date_creation=date_creation), user)
    return CompetitionOut(**new_competition.dict())


@router.get("/{slug}", status_code=200)
def retrieve_competition(slug: str):
    """ return competition associate to the slug """
    competition = get_competition_by_slug(slug)
    print("coucou")
    return competition


@router.get("", status_code=200)
def retrieve_user_competitions(user: User = Depends(
        get_current_user_authorizer())):
    """ return competitions owned by user """

    competitions = get_user_competitions(user)
    return [
        CompetitionOut(**competition.dict()) for competition in competitions
    ]


@router.patch("/{id_competition}", status_code=201)
def patch_competition(id_competition: str,
                      competition: CompetitionIn,
                      user: User = Depends(get_current_user_authorizer())):
    """ modify an existing competition """

    # validate
    is_competition_owner(id_competition, user)

    competition_key = CompetitionKey(id=id_competition)

    current_competition = get_competition(competition_key)

    # update slug if name change
    slug = current_competition.slug
    if competition.name != current_competition.name:
        slug = get_unique_slug(competition.name)

    competition_in_db = CompetitionInDb(**competition.dict(), slug=slug)
    updated_competition = update_competition(competition_key,
                                             competition_in_db)
    return CompetitionOut(**updated_competition.dict())


@router.delete("/{id_competition}", status_code=202)
def destroy_competition(id_competition: str,
                        user: User = Depends(get_current_user_authorizer())):
    """ destroy an existing competition """

    # validate
    is_competition_owner(id_competition, user)

    # delete
    delete_competition(CompetitionKey(id=id_competition))


def get_unique_slug(name: str):
    """ Generate a unique slug for a name """
    slug = slugify(name)
    unique_slug = slug
    num = 1
    while get_competition_by_slug(slug=unique_slug, raise_404=False):
        unique_slug = f"{slug}-{num}"
        num += 1
    return unique_slug
