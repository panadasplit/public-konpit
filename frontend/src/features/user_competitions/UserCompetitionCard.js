import React, { Component } from "react";

import { Link } from "react-router-dom";

import PropTypes from "prop-types";
import DeleteFooterItem from "./DeleteFooterItem";

export default class UserCompetitionCard extends Component {
  static propTypes = {
    competition: PropTypes.object.isRequired
  };

  render() {
    const { name, description, slug } = this.props.competition;
    return (
      <div className="column is-full">
        <div className="box">
          <div className="card">
            <div className="card-content">
              <p className="title">{name}</p>
              <div className="content">
                <p>{description}</p>
              </div>
            </div>
            <footer className="card-footer">
              <DeleteFooterItem competition={this.props.competition} />
              <Link
                className="button is-link is-light
                is-medium card-footer-item"
                to={`/competition/${slug}`}
              >
                Watch
              </Link>
              <Link
                className="button is-primary is-light is-medium card-footer-item"
                to={`/edit-competition/${slug}`}
              >
                Edit
              </Link>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}
