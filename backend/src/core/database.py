""" Firestore Database Module """
from firebase_admin import firestore


class DB():
    """ Firestore Database Singleton"""

    _instance = None
    _client = None

    def __new__(cls, *args, **kwargs):
        if cls._instance is None:
            cls._instance = super(DB, cls).__new__(cls)
        return cls._instance

    def __init__(self, firebase_app=None):
        if self._client is None:
            if firebase_app:
                self._client = firestore.client(firebase_app)
            else:
                self._client = firestore.client()

    @property
    def client(self):
        """ return firestore client """
        return self._client
