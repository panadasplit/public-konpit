import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

// AUTHENTICATION
import { firebaseReducer } from "react-redux-firebase"
import firestoreReducer from 'redux-firestore'
import authReducers from "./features/auth/authReducers";
import registerReducers from "./features/register/registerReducers";

// COMPETITIONS
import userCompetitionsReducers from "./features/user_competitions/userCompetitionsReducers";
import teamUserReducers from "./features/user_teams/userTeamsReducers";
import matchUserReducers from "./features/user_matchs/userMatchsReducers";

const createRootReducer = history =>
  combineReducers({
    router: connectRouter(history),

    auth: authReducers,
    firebase: firebaseReducer,
    firestore: firestoreReducer,
    register: registerReducers,
    userCompetitions: userCompetitionsReducers,
    userTeams: teamUserReducers,
    userMatchs: matchUserReducers
  });
export default createRootReducer;
