""" CRUD functions for competitions """

from google.cloud.firestore_v1.document import DocumentSnapshot

from src.models.match import Match, MatchKey, MatchInDB, MatchInDBCreate
from src.models.team import TeamKey, Team
from src.models.league import LeagueKey

COMPETITIONS = "competitions"
MATCHS = "matchs"


def add_league_match(league_key: LeagueKey, match: MatchInDBCreate) -> Match:
    """ add a new match to a league"""

    new_match_ref = league_key.reference.collection(MATCHS).document()

    new_match_ref.set(match_to_doc_data(match))

    return Match(**match.dict(), id=new_match_ref.id, id_league=league_key.id)


def update_league_match(league_key: LeagueKey, match_key: MatchKey,
                        match: MatchInDB) -> Match:
    """ update a competition """

    match_key.reference.update(match.dict(exclude={"order"}))

    return doc_to_match(match_key.document, league_key)


def match_to_doc_data(match: MatchInDBCreate):
    """ match model to match document data """

    data = match.dict(exclude={"home_team", "away_team"})

    home_team_data = {
        "team": match.home_team.reference,
        "name": match.home_team.name
    }
    away_team_data = {
        "team": match.away_team.reference,
        "name": match.away_team.name
    }

    data.update({"home_team": home_team_data, "away_team": away_team_data})

    return data


def doc_to_match(match: DocumentSnapshot, league_key: LeagueKey) -> Match:
    """ match document to match model """

    dict_match = match.to_dict()

    home_team, away_team = dict_match.pop("home_team"), dict_match.pop(
        "away_team")

    return Match(id=match.id,
                 id_league=league_key.id,
                 home_team=Team(id=home_team.get('team').id,
                                id_competition=league_key.id_competition,
                                name=home_team.get("name")),
                 away_team=Team(id=away_team.get('team').id,
                                id_competition=league_key.id_competition,
                                name=away_team.get("name")),
                 **dict_match)
