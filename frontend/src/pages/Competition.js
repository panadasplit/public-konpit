import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import CompetitionHeader from "../features/competitions/CompetitionHeader";
import Matchs from "../features/competitions/matchs/Matchs";
import Classements from "../features/competitions/classement/Classements";

import { getCompetition } from "../features/user_competitions/actions";

import TabsMenu from "../common/tabs/TabsMenu";
import ContentTab from "../common/tabs/ContentTab";

const TABNAMES = ["Table", "Results"];

class EditUserCompetition extends Component {
  static propTypes = {
    getCompetition: PropTypes.func.isRequired
  };

  state = {
    active_tab: TABNAMES[0]
  };

  componentDidMount() {
    const { slug } = this.props.match.params;
    this.props.getCompetition(slug);
  }


  onTabChange = new_active_tab => {
    this.setState({
      active_tab: new_active_tab
    });
  };

  render() {
    const { active_tab } = this.state;

    return (
      <Fragment>
        <CompetitionHeader competition={this.props.competition} />
        <TabsMenu tabs={TABNAMES} onChange={this.onTabChange} />
        <div className="container">
          <div className="section">
            <ContentTab name={TABNAMES[0]} active_tab={active_tab}>
              <Classements />
            </ContentTab>
            <ContentTab name={TABNAMES[1]} active_tab={active_tab}>
              <Matchs />
            </ContentTab>
          </div>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  competition: state.userCompetitions.competition
});

export default connect(
  mapStateToProps,
  { getCompetition }
)(EditUserCompetition);
