""" Match Models """

from typing import Optional
from pydantic import BaseModel

from src.core.database import DB

from src.models.team import Team, TeamOut


class MatchKey(BaseModel):
    id: str
    id_league: str = None

    @property
    def reference(self):
        """ firestore document reference """
        return DB().client.collection("leagues").document(
            self.id_league).collection("matchs").document(self.id)

    @property
    def document(self):
        """ firestore document snapshot """
        return self.reference.get()


class Match(MatchKey):

    home_team: Team
    away_team: Team
    home_score: Optional[int] = None
    away_score: Optional[int] = None
    order: int


class MatchIn(BaseModel):
    home_score: int
    away_score: int


class MatchInDB(BaseModel):
    home_score: int
    away_score: int
    order: Optional[int] = None


class MatchInDBCreate(BaseModel):
    home_team: Team
    away_team: Team
    home_score: Optional[int] = None
    away_score: Optional[int] = None
    order: int


class MatchOut(MatchKey):
    home_team: TeamOut
    away_team: TeamOut
    home_score: int = None
    away_score: int = None
    order: int