import React, { Component } from "react";

import PropTypes from "prop-types";
import UpdateScoreForm from "./UpdateScoreForm";

export default class UserMatchCard extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired
  };

  render() {
    return (
      <div className="column is-three-fifths is-offset-one-fifth is-full-mobile">
        <div className="box">
          <div className="card">
            <div className="card-content">
              <div className="content">
                <UpdateScoreForm match={this.props.match} />{" "}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
