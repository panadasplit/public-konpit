import React, { Component } from "react";
import PropTypes from "prop-types";

import UserMatchCard from "./UserMatchCard";

export default class ListUserMatchs extends Component {
  static propTypes = {
    matchs: PropTypes.array.isRequired
  };

  render() {
    return (
      <div className="columns is-multiline">
        {this.props.matchs.map(match => (
          <UserMatchCard key={match.id} match={match} />
        ))}
      </div>
    );
  }
}
