import React, { Component, Fragment } from "react";

import { Route, Switch } from "react-router-dom";
import { ConnectedRouter } from "connected-react-router";

import { Provider } from "react-redux";

import store, { history } from "./store";

import { ReactReduxFirebaseProvider } from 'react-redux-firebase'
import { createFirestoreInstance } from 'redux-firestore'
import firebase from './firebase/firebase'

// PRIVATE ROUTE
import PrivateRoute from "./utils/PrivateRoute";

// LAYOUT
import Navbar from "./layout/Navbar";

// PAGES
import Home from "./pages/Home";
// AUTH
import Login from "./pages/Login";
import Register from "./pages/Register";
// COMPETITIONS
import Competition from "./pages/Competition";
import UserCompetitions from "./pages/UserCompetitions";
import NewCompetition from "./pages/NewCompetition";
import EditUserCompetition from "./pages/EditUserCompetition";

import "react-loader-spinner/dist/loader/css/react-spinner-loader.css"


// react-redux-firebase config
const rrfProps = {
  firebase,
  config: { userProfile: 'users', useFirestoreForProfile: true },
  dispatch: store.dispatch,
  createFirestoreInstance
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ReactReduxFirebaseProvider {...rrfProps}>
          <ConnectedRouter history={history}>
            <Fragment>
              <Navbar />
              <Switch>
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/competition/:slug" component={Competition} />
                <PrivateRoute
                  exact
                  path="/my-competitions"
                  component={UserCompetitions}
                />
                <PrivateRoute
                  exact
                  path="/new-competition"
                  component={NewCompetition}
                />
                <PrivateRoute
                  path="/edit-competition/:slug"
                  component={EditUserCompetition}
                />
                <Route component={Home} />
              </Switch>
            </Fragment>
          </ConnectedRouter>
        </ReactReduxFirebaseProvider>
      </Provider>
    );
  }
}

export default App;
