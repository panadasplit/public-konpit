import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getMatchs } from "../../user_matchs/actions";
import generateClassement from "./classement";

import ClassementTableHead from "./ClassementTableHead";
import ClassementTableTeamRow from "./ClassementTableTeamRow";

class Classements extends Component {
  static propTypes = {
    getMatchs: PropTypes.func.isRequired,
    idCompetition: PropTypes.string,
    matchs: PropTypes.array.isRequired
  };

  componentDidUpdate(prevProps) {
    if (this.props.idCompetition !== prevProps.idCompetition) {
      if (this.props.idCompetition) {
        this.props.getMatchs(this.props.idCompetition);
      }
    }
  }

  render() {
    const { matchs } = this.props;
    const teams = generateClassement(matchs);
    return (
      <Fragment>
        <div className="tile">
          <table className="table is-fullwidth">
            <ClassementTableHead />
            <tbody>
              {teams.map((team, id) => (
                <ClassementTableTeamRow key={team.id} pos={id} team={team} />
              ))}
            </tbody>
          </table>
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  idCompetition: state.userCompetitions.competition.id,
  matchs: state.userMatchs.matchs
});

export default connect(
  mapStateToProps,
  { getMatchs }
)(Classements);
