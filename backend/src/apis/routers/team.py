""" Team routers """

from fastapi import APIRouter, Depends

from src.core.validators import is_competition_owner

from src.models.competition import CompetitionKey
from src.models.team import TeamKey, TeamIn, TeamInDB

from src.crud.team import (
    add_team,
    get_competition_teams,
    update_team,
    delete_team,
)

from src.models.user import User

from src.core.authentication import get_current_user_authorizer

router = APIRouter()


@router.post("/{id_competition}", status_code=201)
def create_team(id_competition: str,
                user: User = Depends(get_current_user_authorizer())):
    """ create and return a new team """

    # validate permissions
    is_competition_owner(id_competition, user)

    # init data
    competition = CompetitionKey(id=id_competition)
    team = TeamInDB(name=get_unique_team_name())

    # pass data to crud methode
    added_competition = add_team(competition, team)

    return added_competition


@router.get("/{id_competition}", status_code=200)
def retrieve_competition_teams(id_competition: str):
    """ return all teams in a competition """

    competition = CompetitionKey(id=id_competition)

    teams = get_competition_teams(competition)

    return teams


@router.patch("/{id_competition}/{id_team}", status_code=201)
def modify_team(id_competition: str,
                id_team: str,
                team: TeamIn,
                user: User = Depends(get_current_user_authorizer())):
    """ modify an existing competition """

    is_competition_owner(id_competition, user)

    updated_team = update_team(
        TeamKey(id=id_team, id_competition=id_competition),
        TeamInDB(**team.dict()))

    return updated_team


@router.delete("/{id_competition}/{id_team}", status_code=202)
def destroy_team(id_competition: str,
                 id_team: str,
                 user: User = Depends(get_current_user_authorizer())):
    """ destroy an existing competition """

    is_competition_owner(id_competition, user)

    delete_team(TeamKey(id=id_team, id_competition=id_competition))


def get_unique_team_name():
    """ return a unique team name for the competition"""
    # TODO
    return "new team"
