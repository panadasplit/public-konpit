import React, { Component } from "react";
import { Link } from "react-router-dom";

import RegisterForm from "../features/register/RegisterForm";

export default class Login extends Component {
  render() {
    return (
      <section className="section">
        <div className="container">
          <h1 className="title has-text-centered">Get yourself an account</h1>
          <div className="columns">
            <div className="column is-one-third is-offset-one-third">
              <RegisterForm />
              <br />
              <nav className="level is-mobile">
                <p className="level-item">
                  <Link to="/login">
                    <strong>Already have an account ?</strong>
                  </Link>
                </p>
              </nav>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
