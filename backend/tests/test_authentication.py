""" test authentication """

from fastapi import FastAPI, Depends
from starlette.testclient import TestClient

from firebase_admin.auth import create_custom_token

from src.core.authentication import _get_authorization_token, get_current_user_authorizer, JWT_TOKEN_PREFIX
from src.models.user import User

app = FastAPI()
client = TestClient(app)

custom_token = "2222"


@app.get("/token")
def get_token(token: str = Depends(_get_authorization_token)):
    return token


def test_get_valid_authorization_token():

    response = client.get(
        "/token",
        headers={"Authorization": f"{JWT_TOKEN_PREFIX} {custom_token}"})

    assert response.status_code == 200
    assert response.json() == custom_token


def test_get_invalid_authorization_token():

    response = client.get("/token",
                          headers={"Authorization": f"FAKE_JWT 8888"})

    assert response.status_code == 403


def test_get_none_authorization_token():

    response = client.get("/token")

    assert response.status_code == 422
