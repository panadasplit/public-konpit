""" Main Api Router """

from fastapi import APIRouter
from .routers import competition, team, league

api_router = APIRouter()

api_router.include_router(
    competition.router,
    prefix="/competitions",
    tags=["competitions"],
)
api_router.include_router(team.router, prefix="/teams", tags=["teams"])

api_router.include_router(league.router, prefix="/leagues", tags=["leagues"])
