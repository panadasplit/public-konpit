import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import LogoutButton from "../features/auth/LogoutButton";

class Navbar extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired
  };

  render() {

    const { isLoaded, isEmpty } = this.props.auth

    const guestLinks = (
      <div className="buttons">
        <Link className="button is-primary" to="/register">
          <strong>Sign up</strong>
        </Link>
        <Link className="button is-light" to="/login">
          Log in
        </Link>
      </div>
    );

    const authLinks = (
      <div className="buttons">
        <Link className="button is-light" to="/my-competitions">
          <strong>Competitions</strong>
        </Link>
        <LogoutButton />
      </div>
    );

    let displayLinks = null

    if (!isLoaded) {
      displayLinks = "";
    } else if (isLoaded && isEmpty) {
      displayLinks = guestLinks;
    } else {
      displayLinks = authLinks;
    }

    return (
      <nav
        className="navbar is-primary"
        role="navigation"
        aria-label="main navigation"
      >
        <div className="container">
          <div className="navbar-brand">
            <Link className="navbar-item" to="/">
              <h1 className="title has-text-white	">Konpit</h1>
            </Link>
          </div>

          <div id="navbarBasicExample" className="navbar-menu">
            <div className="navbar-end">
              <div className="navbar-item">
                {displayLinks}
              </div>
            </div>
          </div>
        </div>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.firebase.auth,
  isLoading: state.auth.isLoading
});

export default connect(mapStateToProps)(Navbar);
