import React from "react";

const HowItWorks = () => {
    return (
        <div className="section">
            <div className="container">
                <div className="columns">
                    <div className="column is-one-third">
                        <h1 className="title has-text-centered">Example</h1>
                    </div>
                    <div className="column is-one-third">
                        <h1 className="title has-text-centered">Example</h1>
                    </div>
                    <div className="column is-one-third">
                        <h1 className="title has-text-centered">Example</h1>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default HowItWorks;
