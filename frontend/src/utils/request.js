import axios from "axios";
import config from "../config";
import firebase from "../firebase/firebase"

const request = axios.create({
  baseURL: config.baseAPI
});

request.interceptors.request.use(
  config => {
    if (firebase.auth().currentUser) {
      firebase.auth().currentUser.getIdToken(false).then(token => {
        config.headers.Authorization = `JWT ${token}`;
      })
    }

    config.headers["Content-Type"] = "application/json";

    return config;
  })

export default request;
