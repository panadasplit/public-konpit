import React, { Component, Fragment } from "react";
import { connect } from "react-redux";

import { createNewCompetition } from "./actions";

class CompetitionForm extends Component {
  render() {
    return (
      <Fragment>
        <div className="field">
          <div className="control">
            <label className="label">Name</label>
            <input
              className="input is-large"
              type="text"
              name="name"
              value={this.state.name}
              onChange={this.onChange}
            />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <label className="label">Description</label>
            <textarea
              className="textarea is-large"
              name="description"
              value={this.state.description}
              onChange={this.onChange}
            ></textarea>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(
  null,
  { createNewCompetition }
)(CompetitionForm);
