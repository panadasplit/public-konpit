""" Competition Models """

from datetime import datetime

from typing import List
from enum import Enum

from pydantic import BaseModel

from src.core.database import DB

from .user import User


class CompetitionTypeEnum(str, Enum):
    league = 'league'


class CompetitionKey(BaseModel):
    id: str

    @property
    def reference(self):
        """ return competition document reference """

        return DB().client.collection("competitions").document(self.id)

    @property
    def document(self):
        """ return competition document reference """

        return self.reference.get()


class Competition(CompetitionKey):

    name: str
    description: str
    date_creation: datetime
    slug: str
    competition_type = CompetitionTypeEnum.league
    user: User
    leagues: List[str] = []


class CompetitionIn(BaseModel):

    name: str
    description: str = None
    competition_type: CompetitionTypeEnum = CompetitionTypeEnum.league


class CompetitionOut(CompetitionKey):

    name: str
    description: str = None
    slug: str
    competition_type: CompetitionTypeEnum = CompetitionTypeEnum.league
    leagues: List[str] = []


class CompetitionInDb(CompetitionIn):

    slug: str
    date_creation: datetime = None
