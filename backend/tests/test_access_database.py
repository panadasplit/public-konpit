"""
    Tests for database connections
"""

from src.core.database import DB


def test_init_db(firebase_app):

    assert DB(firebase_app=firebase_app).client is not None


def test_add_collection(firebase_app):
    db = DB(firebase_app=firebase_app).client

    note_ref = db.collection("notes").add({'name': 'koko'})

    assert note_ref is not None