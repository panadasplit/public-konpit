
const URL_API = process.env.NODE_ENV === 'production' ? process.env.REACT_APP_URL_API : ""

const baseAPI = URL_API;

const config = { baseAPI };

export default config;