import React, { Component, Fragment } from "react";

import ListUserCompetitions from "../features/user_competitions/ListUserCompetitions";
import CreateCompetitionButton from "../features/user_competitions/CreateCompetitionButton";

export default class UserCompetitions extends Component {
  render() {
    return (
      <Fragment>
        <div className="container">
          <div className="section">
            <CreateCompetitionButton />
            <ListUserCompetitions />
          </div>
        </div>
      </Fragment>
    );
  }
}
