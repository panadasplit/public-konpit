import React, { Component } from "react";
import { connect } from "react-redux";

import PropTypes from "prop-types";
import DeleteFooterItem from "./DeleteFooterItem";

import { updateTeam } from "./actions";

class UserTeamCard extends Component {
  static propTypes = {
    team: PropTypes.object.isRequired,
    updateTeam: PropTypes.func.isRequired
  };

  state = {
    name: "",
    init: false
  };

  componentDidMount() {
    if (!this.state.init) {
      this.setState({ name: this.props.team.name, init: true });
    }
  }

  onChange = e => {
    e.preventDefault();
    this.setState({
      name: e.target.value
    });
  };

  onClickUpdate = e => {
    e.preventDefault();
    const { id } = this.props.team;
    this.props.updateTeam(this.props.id_competition, id, this.state.name);
  };

  render() {
    return (
      <div className="column is-one-quarter-desktop is-half-tablet">
        <div className="box">
          <div className="card">
            <div className="card-content">
              <input
                className="input is-medium"
                type="text"
                placeholder="Normal input"
                value={this.state.name}
                onChange={this.onChange}
              />
            </div>
            <footer className="card-footer">
              <DeleteFooterItem team={this.props.team} id_competition={this.props.id_competition} />
              <button
                className="button is-primary is-medium card-footer-item"
                onClick={this.onClickUpdate}
              >
                Update
              </button>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { updateTeam }
)(UserTeamCard);
