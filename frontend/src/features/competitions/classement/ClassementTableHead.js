import React from "react";

const ClassementTableHead = () => {
  return (
    <thead>
      <tr>
        <th>Pos</th>
        <th>Team</th>
        <th>P</th>
        <th>W</th>
        <th>D</th>
        <th>L</th>
        <th>GF</th>
        <th>GA</th>
        <th>+/-</th>
        <th>Pts</th>
      </tr>
    </thead>
  );
};

export default ClassementTableHead;
