""" Team Models """

from pydantic import BaseModel

from src.core.database import DB


class TeamKey(BaseModel):
    id: str
    id_competition: str

    @property
    def reference(self):
        """ return document reference """
        return DB().client.collection("competitions").document(
            self.id_competition).collection("teams").document(self.id)

    @property
    def document(self):
        """ return team document snapshot """
        return self.reference.get()


class Team(TeamKey):

    name: str


class TeamIn(BaseModel):

    name: str


class TeamInDB(BaseModel):

    name: str


class TeamInMatchDBCreate(TeamKey):

    name: str


class TeamOut(Team):
    pass
