import { push } from "connected-react-router";

import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAIL
} from "./types";

// LOGIN USER
export const login = (email, password) => (dispatch, getState, getFirebase) => {
  dispatch({
    type: LOGIN_REQUEST
  });

  const firebase = getFirebase()

  firebase.auth().signInWithEmailAndPassword(email, password)
    .then(res => {
      dispatch({
        type: LOGIN_SUCCESS,
      });
    })
    .catch(err => {
      dispatch({
        type: LOGIN_FAIL,
        errorMessage: err.message
      });
    });
};


// LOGOUT USER
export const logout = () => (dispatch, getState, getFirebase) => {

  const firebase = getFirebase()

  firebase.auth().signOut().then(
    dispatch(push("/"))
  )
};
