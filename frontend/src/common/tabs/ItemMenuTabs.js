import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class ItemMenuTabs extends Component {
  render() {
    const { active_tab, name, onClick } = this.props;
    const class_name = active_tab === name ? "is-active" : "";
    return (
      <li className={class_name}>
        <Link to="/" onClick={onClick} name={name}>
          {name}
        </Link>
      </li>
    );
  }
}
