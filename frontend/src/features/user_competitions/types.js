export const GET_USER_COMPETIONS_REQUEST = "GET_USER_COMPETIONS_REQUEST";
export const GET_USER_COMPETIONS_SUCCESS = "GET_USER_COMPETIONS_SUCCESS";
export const GET_USER_COMPETIONS_FAIL = "GET_USER_COMPETIONS_FAIL";

export const GET_COMPETITION_REQUEST = "GET_COMPETITION_REQUEST";
export const GET_COMPETITION_SUCCESS = "GET_COMPETITION_SUCCESS";
export const GET_COMPETITION_FAIL = "GET_COMPETITION_FAIL";

export const CREATE_NEW_COMPETITION_REQUEST = "CREATE_NEW_COMPETITION_REQUEST";
export const CREATE_NEW_COMPETITION_SUCCESS = "CREATE_NEW_COMPETITION_SUCCESS";
export const CREATE_NEW_COMPETITION_FAIL = "CREATE_NEW_COMPETITION_FAIL";

export const UPDATE_COMPETITION_REQUEST = "UPDATE_COMPETITION_REQUEST";
export const UPDATE_COMPETITION_SUCCESS = "UPDATE_COMPETITION_SUCCESS";
export const UPDATE_COMPETITION_FAIL = "UPDATE_COMPETITION_FAIL";

export const DELETE_COMPETITION_REQUEST = "DELETE_COMPETITION_REQUEST";
export const DELETE_COMPETITION_SUCCESS = "DELETE_COMPETITION_SUCCESS";
export const DELETE_COMPETITION_FAIL = "DELETE_COMPETITION_FAIL";
