import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class LinkCompetition extends Component {
  render() {
    const slug = this.props.slug;
    return (
      <div className="card">
        <header className="card-header">
          <p className="card-header-title">Link</p>
        </header>
        <div className="card-content">
          <div className="content has-text-weight-bold has-link-text-black">
            <Link to={`/competition/${slug}`}>{`/competition/${slug}`}</Link>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  slug: state.userCompetitions.competition.slug
});

export default connect(mapStateToProps)(LinkCompetition);
