import React, { Component } from "react";

import NewCompetitionForm from "../features/user_competitions/NewCompetitionForm";

export default class NewCompetition extends Component {
  render() {
    return (
      <div className="section">
        <div className="container">
          <h1 className="title has-text-centered">Start by describing your competition</h1>
          <h2 className="subtitle has-text-centered">
            don't worry, you can change that later
          </h2>
          <NewCompetitionForm />
        </div>
      </div>
    );
  }
}
