""" API main file """

import uvicorn

from fastapi import FastAPI

from starlette.middleware.cors import CORSMiddleware

from firebase_admin import initialize_app

from src.core.database import DB

from src.apis.router import api_router

# init fastapi app
app = FastAPI()
app.include_router(api_router)
# init firebase app
firebase_app = initialize_app()
# init firestore db client
DB(firebase_app=firebase_app)

origins = [
    "https://konpit.com",
    "https://konpit.appspot.com",
    "https://localhost:8000",
    "http://localhost:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def root():
    """ root address """
    return {"message": "Konpit API"}


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
