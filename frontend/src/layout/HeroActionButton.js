import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

export class HeroActionButton extends Component {
  static propTypes = {
    isAuthenticated: PropTypes.bool.isRequired
  };

  render() {

    const { isEmpty } = this.props
    const guest_link = "/register";

    const auth_link = "/my-competitions";

    return (
      <div className="buttons is-centered">
        <Link
          to={!isEmpty ? auth_link : guest_link}
          className="button is-primary is-inverted is-medium"
        >
          Get Started
        </Link>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isEmpty: state.firebase.auth.isEmpty
});

export default connect(mapStateToProps)(HeroActionButton);
