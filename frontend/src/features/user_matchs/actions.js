import request from "../../utils/request";

import {
  CREATE_MATCHS_REQUEST,
  CREATE_MATCHS_SUCCESS,
  CREATE_MATCHS_FAIL,
  GET_MATCHS_REQUEST,
  GET_MATCHS_SUCCESS,
  GET_MATCHS_FAIL,
  UPDATE_MATCH_REQUEST,
  UPDATE_MATCH_SUCCESS,
  UPDATE_MATCH_FAIL
} from "./types";

// CREATE MATCHS
export const createMatchs = id_competition => dispatch => {
  dispatch({
    type: CREATE_MATCHS_REQUEST
  });

  request
    .post("leagues/" + id_competition)
    .then(res => {
      dispatch({
        type: CREATE_MATCHS_SUCCESS,
        matchs: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: CREATE_MATCHS_FAIL,
        errorMessage: err.data
      });
    });
};

// CREATE MATCHS
export const getMatchs = idCompetition => dispatch => {
  dispatch({
    type: GET_MATCHS_REQUEST
  });

  request
    .get("leagues/" + idCompetition)
    .then(res => {
      dispatch({
        type: GET_MATCHS_SUCCESS,
        matchs: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_MATCHS_FAIL,
        errorMessage: err.data
      });
    });
};

// UPDATE MATCHS
export const updateMatch = (idCompetition, idMatch, home_score, away_score) => dispatch => {
  dispatch({
    type: UPDATE_MATCH_REQUEST
  });

  const data = JSON.stringify({ home_score, away_score });

  request
    .patch("leagues/" + idCompetition + "/" + idMatch, data)
    .then(res => {
      dispatch({
        type: UPDATE_MATCH_SUCCESS,
        match: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: UPDATE_MATCH_FAIL,
        errorMessage: err.data
      });
    });
};
