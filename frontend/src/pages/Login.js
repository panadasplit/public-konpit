import React, { Component } from "react";
import { Link } from "react-router-dom";

import LoginForm from "../features/auth/LoginForm";

export default class Login extends Component {
  render() {
    return (
      <section className="section">
        <div className="container">
          <h1 className="title has-text-centered">Welcome Back</h1>
          <div className="columns">
            <div className="column is-one-third is-offset-one-third">
              <LoginForm />
              <br />
              <nav className="level is-mobile">
                <p className="level-item">
                  <Link to="/register">
                    <strong>Not yet a member?</strong>
                  </Link>
                </p>
              </nav>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
