import { configureStore, getDefaultMiddleware } from "redux-starter-kit";
import { routerMiddleware } from "connected-react-router";
import { createHashHistory } from "history";
import { getFirebase } from 'react-redux-firebase';

import createRootReducer from "./indexReducers";

export const history = createHashHistory();

const customizedDefaultMiddleware = getDefaultMiddleware({
  thunk: {
    extraArgument: getFirebase
  },
  serializableCheck: false
})

const middleware = [...customizedDefaultMiddleware, routerMiddleware(history)];

const preloadedState = {};

const enhancers = [];

const store = configureStore({
  reducer: createRootReducer(history),
  middleware,
  devTools: process.env.NODE_ENV !== "production",
  preloadedState,
  enhancers
});

export default store;
