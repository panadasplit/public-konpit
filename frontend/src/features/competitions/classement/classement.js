function initTeam(id, name) {
  return {
    id: id,
    name: name,
    played: 0,
    w: 0,
    d: 0,
    l: 0,
    gf: 0,
    ga: 0,
    pts: 0
  };
}

function resultMatch(match) {
  if (match.home_score && match.away_score) {
    let home_score = parseInt(match.home_score);
    let away_score = parseInt(match.away_score);
    let home_team = {
      id: match.home_team.id,
      name: match.home_team.name,
      gf: home_score,
      ga: away_score,
      r: ""
    };
    let away_team = {
      id: match.away_team.id,
      name: match.away_team.name,
      gf: match.away_score,
      ga: match.home_score,
      r: ""
    };
    if (home_score - away_score > 0) {
      home_team.r = "w";
      away_team.r = "l";
    } else if (home_score - away_score < 0) {
      home_team.r = "l";
      away_team.r = "w";
    } else {
      home_team.r = "d";
      away_team.r = "d";
    }
    return [home_team, away_team];
  }
  return null;
}

function compare(team_a, team_b) {
  if (team_a.pts - team_b.pts === 0) {
    let gda = team_a.gf - team_a.ga;
    let gdb = team_b.gf - team_b.ga;
    if (gda > gdb) {
      return 1;
    } else if (gda < gdb) {
      return -1;
    } else {
      return team_a.name.localeCompare(team_b.name);
    }
  }
  return team_a.pts - team_b.pts;
}

function generateClassements(matchs) {
  const pts_w = 3;
  const pts_d = 1;
  const pts_l = 0;
  let teams = [];
  let id_teams = [];

  // INIT TEAMS
  matchs.forEach(match => {
    [match.home_team, match.away_team].forEach(match_team => {
      if (!id_teams.includes(match_team.id)) {
        teams.push(initTeam(match_team.id, match_team.name));
        id_teams.push(match_team.id);
      }
    });
  });

  // ADD RESULTS
  matchs.forEach(match => {
    let match_teams = resultMatch(match);
    if (match_teams !== null) {
      match_teams.forEach(match_team => {
        let team = teams.find(team => {
          return team.id === match_team.id;
        });

        team.played += 1;
        team.gf += match_team.gf;
        team.ga += match_team.ga;

        switch (match_team.r) {
          case "w":
            team.w += 1;
            break;
          case "l":
            team.l += 1;
            break;
          default:
            team.d += 1;
        }
      });
    }
  });

  teams.map(team => {
    team.pts = team.w * pts_w + team.d * pts_d + team.l * pts_l;
    return team;
  });

  return teams.sort(compare).reverse();
}

export default generateClassements;
