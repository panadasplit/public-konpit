import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { createNewCompetition } from "./actions";

class NewCompetitionForm extends Component {
  static propTypes = {
    createNewCompetition: PropTypes.func.isRequired
  };

  state = {
    name: "",
    description: ""
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.createNewCompetition(this.state.name, this.state.description);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  resetForm = () => {
    this.setState({
      name: "",
      description: ""
    });
  };

  render() {
    return (
      <div className="columns">
        <div className="column"></div>
        <div className="column is-three-quarters">
          <form className="form" onSubmit={this.onSubmit}>
            <div className="field">
              <div className="control">
                <label className="label">Name</label>
                <input
                  className="input is-large"
                  type="text"
                  name="name"
                  value={this.state.name}
                  onChange={this.onChange}
                  placeholder="Awesome Name"
                />
              </div>
            </div>
            <div className="field">
              <div className="control">
                <label className="label">Description</label>
                <textarea
                  className="textarea is-large"
                  placeholder="Great And Short Description"
                  name="description"
                  value={this.state.description}
                  onChange={this.onChange}
                ></textarea>
              </div>
            </div>
            <div className="field is-grouped is-grouped-right">
              <div className="control">
                <button className="button is-primary" type="submit">
                  Create
                </button>
              </div>
              <div className="control">
                <Link className="button is-link is-light" to="/my-competitions">
                  Cancel
                </Link>
              </div>
            </div>
          </form>
        </div>
        <div className="column"></div>
      </div>
    );
  }
}

export default connect(
  null,
  { createNewCompetition }
)(NewCompetitionForm);
