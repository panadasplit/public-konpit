import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getMatchs } from "./actions";

import ListUserMatchs from "./ListUserMatchs";
import CreateMatchsButton from "./CreateMatchsButton";

class EditMatchs extends Component {
  static propTypes = {
    getMatchs: PropTypes.func.isRequired,
    matchs: PropTypes.array.isRequired
  };

  state = {
    matchs_fetched: false
  };

  componentDidMount() {
    if (!this.state.matchs_fetched) {
      this.props.getMatchs(this.props.idCompetition);
      this.setState({ matchs_fetched: true });
    }
  }

  render() {
    const { matchs } = this.props;
    return (
      <Fragment>
        <CreateMatchsButton />
        <div className="section">
          <ListUserMatchs matchs={matchs} />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  matchs: state.userMatchs.matchs,
  idCompetition: state.userCompetitions.competition.id
});

export default connect(
  mapStateToProps,
  { getMatchs }
)(EditMatchs);
