import React from "react";
import { Route, Redirect } from "react-router-dom";
import { connect } from "react-redux";

import Loader from 'react-loader-spinner'


const PrivateRoute = ({ component: Component, auth, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      if (!auth.isLoaded) {
        return (
          <nav className="level">
            <p class="level-item">
              <Loader
                type="ThreeDots"
                color="#851de0"
                height={100}
                width={100}
              />
            </p>
          </nav >
        )
      } else if (auth.isLoaded && auth.isEmpty) {
        return <Redirect to="/login" />;
      } else {
        return <Component {...props} />;
      }
    }}
  />
);

const mapStateToProps = state => ({
  auth: state.firebase.auth,
});

export default connect(mapStateToProps)(PrivateRoute);
