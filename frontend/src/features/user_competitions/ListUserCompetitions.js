import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getUserCompetitions } from "./actions";

import UserCompetitionCard from "./UserCompetitionCard";

class ListUserCompetitions extends Component {
  static propTypes = {
    getUserCompetitions: PropTypes.func.isRequired,
    competitions: PropTypes.array.isRequired
  };

  componentDidMount() {
    this.props.getUserCompetitions();
  }

  render() {
    return (
      <section>
        <div className="section">
          <div className="columns is-multiline">
            {this.props.competitions.map(competition => (
              <UserCompetitionCard
                key={competition.id}
                competition={competition}
              />
            ))}
          </div>
        </div>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  competitions: state.userCompetitions.competitions
});

export default connect(
  mapStateToProps,
  { getUserCompetitions }
)(ListUserCompetitions);
