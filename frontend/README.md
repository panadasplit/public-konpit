# Konpit Frontend 

## TODOS

* add forms validations
* add authentication features : (change password, valdiate with email ...)
* add notifications
* add product explanation on landing page 
* add new competition type
* review the architecture of src folder

## Technologies

Started with create-react-app

React / Redux as JS frameworks

Bulma.io as CSS framework

## Install

`npm install`

## Build

`make build`

## Deploy

`make deploy`

## Run Locally

`npm start`

## Setup Firebase:
* create src/firebase/firebaseConfig.js
* import your firebase config for dev and prod and import them in src/firebase/firebase.js