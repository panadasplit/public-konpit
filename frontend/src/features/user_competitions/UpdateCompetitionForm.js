import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { updateCompetition } from "./actions";

class UpdateCompetitionForm extends Component {
  static propTypes = {
    updateCompetition: PropTypes.func.isRequired
  };

  state = {
    name: "",
    description: ""
  };

  componentDidMount() {
    this.setState({
      ...this.props.competition
    });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.competition !== this.props.competition) {
      this.setState({
        ...this.props.competition
      });
    }
  }

  onSubmit = e => {
    e.preventDefault();
    this.props.updateCompetition(
      this.props.competition.id,
      this.state.name,
      this.state.description
    );
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  // resetForm = () => {
  //   this.setState({
  //     name: "",
  //     description: ""
  //   });
  // };

  render() {
    return (
      <form className="form" onSubmit={this.onSubmit}>
        <div className="field">
          <div className="control">
            <label className="label">Name</label>
            <input
              className="input is-large"
              type="text"
              name="name"
              value={this.state.name}
              onChange={this.onChange}
              placeholder="Large input"
            />
          </div>
        </div>
        <div className="field">
          <div className="control">
            <label className="label">Description</label>
            <textarea
              className="textarea is-large"
              placeholder="e.g. Hello world"
              name="description"
              value={this.state.description}
              onChange={this.onChange}
            ></textarea>
          </div>
        </div>
        <div className="field is-grouped is-grouped-right">
          <div className="control">
            <button className="button is-primary" type="submit">
              Update
            </button>
          </div>
          {/* <div className="control">
                <Link className="button is-link is-light" to="/my-competitions">
                  Cancel
                </Link>
              </div> */}
        </div>
      </form>
    );
  }
}

const mapStateToProps = state => ({
  competition: state.userCompetitions.competition
});

export default connect(
  mapStateToProps,
  { updateCompetition }
)(UpdateCompetitionForm);
