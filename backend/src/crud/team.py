""" CRUD functions for Teams """

from typing import List

from google.cloud.firestore_v1.document import DocumentSnapshot

from src.models.competition import CompetitionKey
from src.models.team import Team, TeamKey, TeamInDB

TEAMS = "teams"


def get_team(team: TeamKey, competition: CompetitionKey) -> Team:
    """ get a team """
    return doc_to_team(team.document, competition)


def get_teams_docs(competition: CompetitionKey) -> List[DocumentSnapshot]:
    """ return teams documents """
    return competition.reference.collection(TEAMS).stream()


def get_competition_teams(competition: CompetitionKey) -> List[Team]:
    """ return all competitions related to user """

    return [
        doc_to_team(team_doc, competition)
        for team_doc in competition.reference.collection(TEAMS).stream()
    ]


def add_team(competition: CompetitionKey, team: TeamInDB) -> Team:
    """ add a new team """

    new_team_ref = competition.reference.collection(TEAMS).document()

    # add competition to database
    new_team_ref.set(team.dict())

    return Team(**team.dict(),
                id=new_team_ref.id,
                id_competition=competition.id)


def update_team(team_key: TeamKey, team: TeamInDB) -> Team:
    """ update a competition """

    team_key.reference.update(team.dict())

    return Team(**team_key.dict(), **team.dict())


def delete_team(team: TeamKey):
    """ delete a competition """

    team.reference.delete()


def doc_to_team(team_doc: DocumentSnapshot,
                competition: CompetitionKey) -> Team:
    """ team document to team model """
    return Team(id=team_doc.id,
                id_competition=competition.id,
                **team_doc.to_dict())
