import {
  GET_TEAMS_REQUEST,
  GET_TEAMS_SUCCESS,
  GET_TEAMS_FAIL,
  GET_TEAM_REQUEST,
  GET_TEAM_SUCCESS,
  GET_TEAM_FAIL,
  CREATE_NEW_TEAM_REQUEST,
  CREATE_NEW_TEAM_SUCCESS,
  CREATE_NEW_TEAM_FAIL,
  UPDATE_TEAM_REQUEST,
  UPDATE_TEAM_SUCCESS,
  UPDATE_TEAM_FAIL,
  DELETE_TEAM_REQUEST,
  DELETE_TEAM_SUCCESS,
  DELETE_TEAM_FAIL
} from "./types";

const initialState = {
  teams: [],
  team: {
    id: 0,
    name: "",
    description: "",
    slug: ""
  },
  isLoading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_TEAMS_REQUEST:
      return { ...state, isLoading: true };
    case GET_TEAMS_SUCCESS:
      return { ...state, isLoading: false, teams: action.teams };
    case GET_TEAMS_FAIL:
      return { ...state, isLoading: false, competitions: [] };
    case GET_TEAM_REQUEST:
      return { ...state, isLoading: true };
    case GET_TEAM_SUCCESS:
      return { ...state, isLoading: false, team: action.team };
    case GET_TEAM_FAIL:
      return { ...state, isLoading: false };
    case CREATE_NEW_TEAM_REQUEST:
      return { ...state, isLoading: true };
    case CREATE_NEW_TEAM_SUCCESS:
      return {
        ...state,
        isLoading: false,
        teams: [...state.teams, action.team]
      };
    case CREATE_NEW_TEAM_FAIL:
      return { ...state, isLoading: false };
    case UPDATE_TEAM_REQUEST:
      return { ...state, isLoading: true };
    case UPDATE_TEAM_SUCCESS:
      const new_update_teams = state.teams.map(team => {
        return team.id === action.team.id ? action.team : team;
      });
      return { ...state, isLoading: false, teams: new_update_teams };
    case UPDATE_TEAM_FAIL:
      return { ...state, isLoading: false };
    case DELETE_TEAM_REQUEST:
      return { ...state, isLoading: true };
    case DELETE_TEAM_SUCCESS:
      const new_teams = state.teams.filter(team => team.id !== action.id_team);
      return { ...state, isLoading: false, teams: new_teams };
    case DELETE_TEAM_FAIL:
      return { ...state, isLoading: false };
    default:
      return state;
  }
}
