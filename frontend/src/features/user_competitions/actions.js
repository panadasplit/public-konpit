import { push } from "connected-react-router";
import request from "../../utils/request";

import {
  GET_USER_COMPETIONS_REQUEST,
  GET_USER_COMPETIONS_SUCCESS,
  GET_USER_COMPETIONS_FAIL,
  GET_COMPETITION_REQUEST,
  GET_COMPETITION_SUCCESS,
  GET_COMPETITION_FAIL,
  CREATE_NEW_COMPETITION_REQUEST,
  CREATE_NEW_COMPETITION_SUCCESS,
  CREATE_NEW_COMPETITION_FAIL,
  UPDATE_COMPETITION_REQUEST,
  UPDATE_COMPETITION_SUCCESS,
  UPDATE_COMPETITION_FAIL,
  DELETE_COMPETITION_REQUEST,
  DELETE_COMPETITION_SUCCESS,
  DELETE_COMPETITION_FAIL
} from "./types";

const url = "competitions";

// GET USER COMPETITIONS
export const getUserCompetitions = () => dispatch => {
  dispatch({
    type: GET_USER_COMPETIONS_REQUEST
  });

  request
    .get(url)
    .then(res => {
      dispatch({
        type: GET_USER_COMPETIONS_SUCCESS,
        competitions: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_USER_COMPETIONS_FAIL,
        errorMessage: err.data
      });
    });
};

// GET USER COMPETITIONS
export const getCompetition = slug => dispatch => {
  dispatch({
    type: GET_COMPETITION_REQUEST
  });

  request
    .get(url + '/' + slug)
    .then(res => {
      dispatch({
        type: GET_COMPETITION_SUCCESS,
        competition: res.data
      });
    })
    .catch(err => {
      console.log("yolo")
      dispatch({
        type: GET_COMPETITION_FAIL,
        errorMessage: err.data
      });
    });
};

// CREATE NEW COMPETITION
export const createNewCompetition = (name, description) => dispatch => {
  dispatch({
    type: CREATE_NEW_COMPETITION_REQUEST
  });

  // Request Body
  const data = JSON.stringify({ name, description });

  request
    .post(url, data)
    .then(res => {
      dispatch({
        type: CREATE_NEW_COMPETITION_SUCCESS,
        competition: res.data
      });
      dispatch(push("/my-competitions"));
    })
    .catch(err => {
      dispatch({
        type: CREATE_NEW_COMPETITION_FAIL,
        errorMessage: err.data
      });
    });
};

// CREATE NEW COMPETITION
export const updateCompetition = (id, name, description) => dispatch => {
  dispatch({
    type: UPDATE_COMPETITION_REQUEST
  });

  // Request Body
  const data = JSON.stringify({ id, name, description });

  request
    .patch(url + "/" + id, data)
    .then(res => {
      dispatch({
        type: UPDATE_COMPETITION_SUCCESS,
        competition: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: UPDATE_COMPETITION_FAIL,
        errorMessage: err.data
      });
    });
};

export const deleteCompetition = (id) => dispatch => {
  dispatch({
    type: DELETE_COMPETITION_REQUEST
  });

  request
    .delete(url + "/" + id)
    .then(res => {
      dispatch({
        type: DELETE_COMPETITION_SUCCESS,
        id_competition: id
      });
    })
    .catch(err => {
      dispatch({
        type: DELETE_COMPETITION_FAIL,
        errorMessage: err.data
      });
    });
};
