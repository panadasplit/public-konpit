import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import UpdateCompetitionForm from "../features/user_competitions/UpdateCompetitionForm";
import EditTeams from "../features/user_teams/EditTeams";
import EditMatchs from "../features/user_matchs/EditMatchs";

import { getCompetition } from "../features/user_competitions/actions";

import TabsMenu from "../common/tabs/TabsMenu";
import ContentTab from "../common/tabs/ContentTab";
import LinkCompetition from "../features/user_competitions/LinkCompetition";

const TABNAMES = ["Competition", "Teams", "Matchs"];

class EditUserCompetition extends Component {
  static propTypes = {
    getCompetition: PropTypes.func.isRequired
  };

  state = {
    active_tab: TABNAMES[0]
  };

  componentDidMount() {
    const { slug } = this.props.match.params;
    this.props.getCompetition(slug);
  }


  onTabChange = new_active_tab => {
    this.setState({
      active_tab: new_active_tab
    });
  };

  render() {
    const { active_tab } = this.state;
    return (
      <Fragment>
        <TabsMenu tabs={TABNAMES} onChange={this.onTabChange} />
        <div className="container">
          <div className="section">
            <ContentTab name="Competition" active_tab={active_tab}>
              <div className="columns">
                <div className="column"></div>
                <div className="column is-three-quarters">
                  <LinkCompetition />
                  <br />
                  <UpdateCompetitionForm />
                </div>
                <div className="column"></div>
              </div>
            </ContentTab>
            <ContentTab name="Teams" active_tab={active_tab}>
              <EditTeams />
            </ContentTab>
            <ContentTab name="Matchs" active_tab={active_tab}>
              <EditMatchs />
            </ContentTab>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(
  null,
  { getCompetition }
)(EditUserCompetition);
