import {
  CREATE_MATCHS_REQUEST,
  CREATE_MATCHS_SUCCESS,
  CREATE_MATCHS_FAIL,
  GET_MATCHS_REQUEST,
  GET_MATCHS_SUCCESS,
  GET_MATCHS_FAIL,
  UPDATE_MATCH_REQUEST,
  UPDATE_MATCH_SUCCESS,
  UPDATE_MATCH_FAIL
} from "./types";

const initialState = {
  matchs: [],
  isLoading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CREATE_MATCHS_REQUEST:
      return { ...state, isLoading: true };
    case CREATE_MATCHS_SUCCESS:
      return { ...state, isLoading: false, matchs: action.matchs };
    case CREATE_MATCHS_FAIL:
      return { ...state, isLoading: false, matchs: [] };
    case GET_MATCHS_REQUEST:
      return { ...state, isLoading: true };
    case GET_MATCHS_SUCCESS:
      return { ...state, isLoading: false, matchs: action.matchs };
    case GET_MATCHS_FAIL:
      return { ...state, isLoading: false };
    case UPDATE_MATCH_REQUEST:
      return { ...state, isLoading: true };
    case UPDATE_MATCH_SUCCESS:
      const new_update_matchs = state.matchs.map(match => {
        return match.id === action.match.id ? action.match : match;
      });
      return {
        ...state,
        isLoading: false,
        matchs: new_update_matchs
      };
    case UPDATE_MATCH_FAIL:
      return { ...state, isLoading: false };
    default:
      return state;
  }
}
