# pylint: disable=redefined-outer-name
""" pytest fixtures """
import pytest

from datetime import datetime

from firebase_admin import initialize_app, delete_app, firestore
from firebase_admin.auth import create_user, delete_user

from faker import Faker

from src.models.competition import Competition
from src.models.user import User


@pytest.fixture(scope="module")
def firebase_app():
    """ Returns an App initialized """
    app = initialize_app()
    yield app
    delete_app(app)


@pytest.fixture(scope="module")
def db_client(firebase_app):
    """ return Database client initialized """
    client = firestore.client(firebase_app)
    return client


@pytest.fixture(scope="function")
def auth_user(firebase_app):
    """ Return an authenticated user """
    faker = Faker()
    user = create_user(email=faker.email(), app=firebase_app)
    yield user
    delete_user(user.uid)


@pytest.fixture(scope="function")
def mock_competition():
    faker = Faker()
    competition = Competition(name=faker.name(),
                              description=faker.text(),
                              date_creation=datetime.now(),
                              competition_type="league",
                              slug=faker.slug(),
                              user=User(email=faker.email(), uid=faker.uuid()))
    return competition
