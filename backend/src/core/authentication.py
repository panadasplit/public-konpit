from typing import Optional

from fastapi import Depends, Header

from starlette.exceptions import HTTPException
from starlette.status import HTTP_403_FORBIDDEN

from firebase_admin.auth import verify_id_token
from firebase_admin.auth import (ExpiredIdTokenError, InvalidIdTokenError,
                                 RevokedIdTokenError, CertificateFetchError)

from src.models.user import User

JWT_TOKEN_PREFIX = "JWT"


def _get_authorization_token(authorization: str = Header(...)):
    if authorization:
        token_prefix, token = authorization.split(" ")
        if token_prefix != JWT_TOKEN_PREFIX:
            raise HTTPException(status_code=HTTP_403_FORBIDDEN,
                                detail="Wrong Authorization header")
    else:
        raise HTTPException(status_code=HTTP_403_FORBIDDEN,
                            detail="Authorization header missing")

    return token


def _get_current_user(token: str = Depends(_get_authorization_token)) -> User:
    user = None
    try:
        payload = verify_id_token(token)
    except (ValueError, ExpiredIdTokenError, InvalidIdTokenError,
            RevokedIdTokenError, CertificateFetchError):
        raise HTTPException(status_code=HTTP_403_FORBIDDEN,
                            detail="Credentials Invalid")
    user = User(uid=payload["uid"])
    return user


def _get_authorization_token_optional(authorization: str = Header(None)):
    if authorization:
        return _get_authorization_token(authorization)
    return None


def _get_current_user_optional(
        token: str = Depends(_get_authorization_token_optional),
) -> Optional[User]:
    if token:
        return _get_current_user(token)

    return None


def get_current_user_authorizer(*, required: bool = True):
    """ return current user, according to the jwtoken in the authroization header """
    if required:
        return _get_current_user

    return _get_current_user_optional
