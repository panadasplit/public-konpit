import {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGIN,
  LOGOUT
} from "./types";

const initialState = {
  isAuthenticating: false,
  isAuthenticated: false,
  errorMessage: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    case LOGIN_REQUEST:
      return {
        ...state,
        isAuthenticating: true,
        errorMessage: null
      };
    case LOGIN_FAIL:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: false,
        errorMessage: action.errorMessage
      };
    case LOGIN:
    case LOGIN_SUCCESS:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: true,
        errorMessage: null
      };
    case LOGOUT:
      return {
        ...state,
        isAuthenticating: false,
        isAuthenticated: false,
        errorMessage: null
      };
    default:
      return state;
  }
}
