
import firebase from "firebase/app";
import 'firebase/firestore'
import 'firebase/auth'

import { firebaseConfigProd, firebaseConfigDev } from "./firebaseConfig"

firebase.initializeApp(process.env.NODE_ENV === 'production' ? firebaseConfigProd : firebaseConfigDev);
firebase.firestore()

export default firebase