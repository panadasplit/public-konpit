import { SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_FAIL } from "./types";

const initialState = {
  isLoading: false,
  isRegistered: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SIGNUP_REQUEST:
      return { ...state, isLoading: true, isRegistered: false };
    case SIGNUP_SUCCESS:
      return { ...state, isLoading: false, isRegistered: true };
    case SIGNUP_FAIL:
      return { ...state, isLoading: false, isRegistered: false };
    default:
      return state;
  }
}
