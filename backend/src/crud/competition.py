""" CRUD functions for competitions """
from typing import List, Union

from google.cloud.firestore_v1.document import DocumentReference
from google.cloud.firestore_v1.document import DocumentSnapshot

from google.api_core.datetime_helpers import from_rfc3339_nanos

from fastapi import HTTPException

from src.core.database import DB

from src.models.user import User
from src.models.competition import CompetitionKey, Competition, CompetitionOut, CompetitionInDb
from src.models.league import LeagueKey
from src.models.team import TeamKey

from src.crud.league import delete_league
from src.crud.team import delete_team

COMPETITIONS = "competitions"


def get_competition(competition: CompetitionKey,
                    raise_404: bool = True) -> Union[Competition, None]:
    """ return a competition according to the slug """

    doc = competition.document

    if not doc.exists:
        if raise_404:
            raise HTTPException(status_code=404, detail="object not found")
        return None

    return doc_to_competition(doc)


def get_competition_by_slug(slug: str, raise_404: bool = True
                            ) -> Union[Competition, None]:
    """ return a competition according to the slug """

    competitions = DB().client.collection(COMPETITIONS).where(
        "slug", "==", slug).limit(1).stream()

    competition = next(competitions, None)

    if competition is None:
        if raise_404:
            raise HTTPException(status_code=404, detail="object not found")
        return None

    return doc_to_competition(competition)


def get_user_competitions(user: User) -> List[CompetitionOut]:
    """ return all competitions related to user """

    user_competitions = DB().client.collection(COMPETITIONS).where(
        "user", "==", user.reference).stream()

    return [
        doc_to_competition(competition) for competition in user_competitions
    ]


def add_competition(competition: CompetitionInDb,
                    user: User) -> CompetitionOut:
    """ add a new competition """

    data = {**competition.dict(), "user": user.reference}

    # init document reference
    new_competition_ref = DB().client.collection(COMPETITIONS).document()
    new_competition_ref.set(data)

    return Competition(**competition.dict(),
                       id=new_competition_ref.id,
                       user=user)


def update_competition(competition_key: CompetitionKey,
                       competition: CompetitionInDb) -> Competition:
    """ update a competition """

    # update competition in database
    competition_key.reference.update(
        competition.dict(exclude={"date_creation"}))

    return doc_to_competition(competition_key.document)


def delete_competition(competition: CompetitionKey):
    """ delete a competition """

    doc = competition.document

    # delete competition childs : leagues / teams
    for league_ref in doc.to_dict().get("leagues", []):
        delete_league(
            LeagueKey(id=league_ref.id, id_competition=competition.id))
    for team_doc in competition.reference.collection("teams").stream():
        delete_team(TeamKey(id=team_doc.id, id_competition=competition.id))

    doc.reference.delete()

    return competition


def doc_to_competition(competition_doc: DocumentSnapshot) -> Competition:
    """ cast document snapshot to competition model """

    competition_dict = competition_doc.to_dict()
    user = competition_dict.pop("user")
    date_creation = competition_dict.pop("date_creation")
    leagues_refs = competition_dict.get("leagues", None)
    if leagues_refs:
        leagues = [league_ref.id for league_ref in leagues_refs]
        competition_dict.update({"leagues": leagues})

    competition_dict.update({
        "date_creation":
        from_rfc3339_nanos(date_creation.rfc3339()),
        "user":
        User(uid=user.id)
    })
    return Competition(id=competition_doc.id, **competition_dict)
