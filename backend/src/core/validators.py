#p
""" Validators """

from starlette.exceptions import HTTPException

from src.models.user import User
from src.models.competition import CompetitionKey


def is_competition_owner(id_competition: str, user: User):
    """ Raise an excepetion if competition is not owned by user """

    user_competition = CompetitionKey(
        id=id_competition).document.to_dict().get("user")

    if user_competition != user.reference:
        raise HTTPException(status_code=403, detail="Forbiden")


def is_league_in_competition(id_league: str, id_competition: str):
    """ is this league belonging to this competition """
    competition = CompetitionKey(id=id_competition).document
    if competition.get("leagues") and not any([
            id_league == id_league_competition
            for id_league_competition in competition.leagues
    ]):
        raise HTTPException(status_code=403, detail="Forbiden")
