import React, { Component } from "react";
import PropTypes from "prop-types";
import ItemMenuTabs from "./ItemMenuTabs";

export default class TabsMenu extends Component {
  static propTypes = {
    tabs: PropTypes.array.isRequired,
    onChange: PropTypes.func.isRequired
  };

  state = {
    active_tab: this.props.tabs[0]
  };

  onClick = e => {
    e.preventDefault();
    this.setState({
      active_tab: e.target.name
    });
    this.props.onChange(e.target.name);
  };

  render() {
    const active_tab = this.state.active_tab;
    const { tabs } = this.props;
    return (
      <div className="tabs is-centered is-large has-background-primary	">
        <ul>
          {tabs.map((tab, id) => (
            <ItemMenuTabs
              onClick={this.onClick}
              active_tab={active_tab}
              name={tab}
              key={id}
            />
          ))}
        </ul>
      </div>
    );
  }
}
