import { Component } from "react";

export default class ContentTab extends Component {
  render() {
    const { active_tab, name } = this.props;
    return active_tab === name ? this.props.children : "";
  }
}
