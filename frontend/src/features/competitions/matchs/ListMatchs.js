import React, { Component } from "react";
import PropTypes from "prop-types";

import MatchCard from "./MatchCard";

export default class ListMatchs extends Component {
  static propTypes = {
    matchs: PropTypes.array.isRequired
  };

  render() {
    return (
      <div className="columns is-multiline">
        {this.props.matchs.map(match => (
          <MatchCard key={match.id} match={match} />
        ))}
      </div>
    );
  }
}
