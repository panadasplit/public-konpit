import React, { Component } from "react";
import PropTypes from "prop-types";

import UserTeamCard from "./UserTeamCard";

export default class ListUserTeams extends Component {
  static propTypes = {
    teams: PropTypes.array.isRequired
  };

  render() {
    const { teams, id_competition } = this.props
    return (
      <div className="columns is-multiline">
        {teams.map(team => (
          <UserTeamCard key={team.id} team={team} id_competition={id_competition} />
        ))}
      </div>
    );
  }
}
