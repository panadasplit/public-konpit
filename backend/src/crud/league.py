""" CRUD functions for competitions """

from operator import attrgetter

from typing import List

from google.cloud import firestore
from google.cloud.firestore_v1.document import DocumentReference
from google.cloud.firestore_v1.document import DocumentSnapshot

from fastapi import HTTPException

from src.core.database import DB

from src.models.competition import CompetitionKey
from src.models.match import Match
from src.models.league import League, LeagueKey

from src.crud.match import doc_to_match

COMPETITIONS = "competitions"


def get_league(league: LeagueKey) -> League:
    """ return a league """

    doc = league.document
    if not doc.exists:
        raise HTTPException(status_code=404, detail="Object Not Found")

    return doc_to_league(doc, league)


def create_league(competition: CompetitionKey, teams=None) -> League:
    """ create a league """

    dict_data = {}

    if not teams:
        # take team from current competition
        dict_data.update({
            "teams": [
                team.reference
                for team in competition.reference.collection("teams").stream()
            ]
        })

    new_league_ref = DB().client.collection("leagues").document()
    new_league_ref.set(dict_data)

    competition.reference.update(
        {"leagues": firestore.ArrayUnion([new_league_ref])})

    return doc_to_league(
        new_league_ref.get(),
        LeagueKey(id=new_league_ref.id, id_competition=competition.id))


def get_competition_leagues_keys(competition: CompetitionKey
                                 ) -> List[LeagueKey]:
    """ return all leagues keys in a competition """

    return [
        LeagueKey(id=league_ref.id, id_competition=competition.id)
        for league_ref in competition.document.to_dict().get("leagues", [])
    ]


def get_league_matchs(league_key: LeagueKey) -> List[Match]:
    """ return all competitions related to user """

    matchs_docs = league_key.reference.collection("matchs").stream()

    return sorted(
        [doc_to_match(match_doc, league_key) for match_doc in matchs_docs],
        key=attrgetter("order"))


def delete_league_matchs(league: LeagueKey):
    """ delete all matchs in a league """

    for match in league.reference.collection("matchs").stream():
        match.reference.delete()


def delete_league(league: LeagueKey):
    """ delete a competition """

    # TODO delete reference in competition

    # delete league childs
    delete_league_matchs(league)

    league.reference.delete()


def doc_to_league(league: DocumentSnapshot, league_key: LeagueKey) -> League:
    """ league document to match model """

    league_matchs = get_league_matchs(league_key)

    return League(
        **league_key.dict(),
        **league.to_dict(),
        matchs=league_matchs,
    )
