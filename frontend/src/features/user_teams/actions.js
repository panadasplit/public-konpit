import request from "../../utils/request";

import {
  GET_TEAMS_REQUEST,
  GET_TEAMS_SUCCESS,
  GET_TEAMS_FAIL,
  CREATE_NEW_TEAM_REQUEST,
  CREATE_NEW_TEAM_SUCCESS,
  CREATE_NEW_TEAM_FAIL,
  UPDATE_TEAM_REQUEST,
  UPDATE_TEAM_SUCCESS,
  UPDATE_TEAM_FAIL,
  DELETE_TEAM_REQUEST,
  DELETE_TEAM_SUCCESS,
  DELETE_TEAM_FAIL
} from "./types";

// GET TEAMS
export const getTeams = id_competition => dispatch => {
  dispatch({
    type: GET_TEAMS_REQUEST
  });

  request
    .get("teams/" + id_competition)
    .then(res => {
      dispatch({
        type: GET_TEAMS_SUCCESS,
        teams: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: GET_TEAMS_FAIL,
        errorMessage: err.data
      });
    });
};

// CREATE NEW TEAM
export const createNewTeam = id_competition => dispatch => {
  dispatch({
    type: CREATE_NEW_TEAM_REQUEST
  });


  request
    .post("teams/" + id_competition)
    .then(res => {
      dispatch({
        type: CREATE_NEW_TEAM_SUCCESS,
        team: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: CREATE_NEW_TEAM_FAIL,
        errorMessage: err.data
      });
    });
};

// UPDATE TEAM
export const updateTeam = (id_competition, id_team, name) => dispatch => {
  dispatch({
    type: UPDATE_TEAM_REQUEST
  });

  // Request Body
  const data = JSON.stringify({ name });

  request
    .patch("teams/" + id_competition + "/" + id_team, data)
    .then(res => {
      dispatch({
        type: UPDATE_TEAM_SUCCESS,
        team: res.data
      });
    })
    .catch(err => {
      dispatch({
        type: UPDATE_TEAM_FAIL,
        errorMessage: err.data
      });
    });
};

// DELETE TEAM
export const deleteTeam = (id_competition, id_team) => dispatch => {
  dispatch({
    type: DELETE_TEAM_REQUEST
  });

  request
    .delete("teams/" + id_competition + "/" + id_team)
    .then(res => {
      dispatch({
        type: DELETE_TEAM_SUCCESS,
        id_team: id_team
      });
    })
    .catch(err => {
      dispatch({
        type: DELETE_TEAM_FAIL,
        errorMessage: err.data
      });
    });
};
