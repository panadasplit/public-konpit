import React from "react";

const ClassementTableTeamRow = props => {
  const { team, pos } = props;
  return (
    <tr>
      <th>{pos + 1}</th>
      <td className="is-capitalized">{`${team.name}`}</td>
      <td>{`${team.played}`}</td>
      <td>{`${team.w}`}</td>
      <td>{`${team.d}`}</td>
      <td>{`${team.l}`}</td>
      <td>{`${team.gf}`}</td>
      <td>{`${team.ga}`}</td>
      <td>{`${team.gf - team.ga}`}</td>
      <td>{`${team.pts}`}</td>
    </tr>
  );
};

export default ClassementTableTeamRow;
