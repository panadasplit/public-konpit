import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { updateMatch } from "./actions";

class UpdateScoreForm extends Component {
  static propTypes = {
    updateMatch: PropTypes.func.isRequired
  };

  state = {
    home_score: "",
    away_score: "",
    fetched: false
  };

  componentDidMount() {
    if (!this.state.fetched) {
      const { home_score, away_score } = this.props.match;
      this.setState({
        home_score: home_score ? home_score : "",
        away_score: away_score ? away_score : "",
        fetched: true
      });
    }
  }

  onSubmit = e => {
    e.preventDefault();
    const { home_score, away_score } = this.state;
    this.props.updateMatch(this.props.idCompetition, this.props.match.id, home_score, away_score);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const { home_score, away_score } = this.state;
    const { match } = this.props;
    //TODO ADAPT FOR MOBILE
    return (
      <section>
        <form onSubmit={this.onSubmit}>
          <div className="columns is-vcentered is-capitalized has-text-weight-bold is-size-6-mobile is-inline-flex">
            <div className="column is-one-third">
              <div className="content">
                <p className="has-text-left ">{match.home_team.name}</p>
              </div>
            </div>
            <div className="column is-one-third">
              <div className="field is-grouped is-grouped-centered has-text-centered is-size-4">
                <p className="control is-expanded">
                  <input
                    className="input is-medium"
                    value={home_score}
                    onChange={this.onChange}
                    name="home_score"
                    placeholder="0"
                  />
                </p>
                {`-`}&nbsp;&nbsp;
                <p className="control is-expanded">
                  <input
                    className="input is-medium"
                    value={away_score}
                    onChange={this.onChange}
                    name="away_score"
                    placeholder="0"
                  />
                </p>
              </div>
            </div>
            <div className="column is-one-third">
              <div className="content">
                <p className="has-text-right is-capitalized">
                  {match.away_team.name}
                </p>
              </div>
            </div>
          </div>
          <div className="field is-grouped is-grouped-right">
            <p className="control">
              <button type="submit" className="button is-primary">
                Save
              </button>
            </p>
          </div>
        </form>
      </section>
    );
  }
}


const mapStateToProps = state => ({
  idCompetition: state.userCompetitions.competition.id
});

export default connect(
  mapStateToProps,
  { updateMatch }
)(UpdateScoreForm);
