import React, { Component } from "react";
import { Link } from "react-router-dom";

export default class CreateCompetitionButton extends Component {
  render() {
    return (
      <Link
        className="button is-large is-fullwidth is-primary"
        to="/new-competition"
      >
        New Competition
      </Link>
    );
  }
}
