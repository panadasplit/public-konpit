import React from "react";
import ActionButton from "./HeroActionButton";

const HomeHero = () => {
  return (
    <section className="hero is-primary is-large">
      <div className="hero-body">
        <div className="container">
          <h1 className="title is-1 has-text-centered">Create Competitions</h1>
          <h2 className="subtitle has-text-centered">
            free you from the management of rankings and scores
          </h2>
          <ActionButton />
        </div>
      </div>
    </section>
  );
};

export default HomeHero;
