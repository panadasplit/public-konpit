import React, { Component, Fragment } from "react";

import HomeHero from "../layout/HomeHero";
// import HowItWorks from "../layout/HowItWorks"

export default class Home extends Component {
  render() {
    return (
      <Fragment>
        <HomeHero />
        {/* <HowItWorks /> */}
      </Fragment>
    )
  }
}
