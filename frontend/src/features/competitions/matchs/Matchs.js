import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getMatchs } from "../../user_matchs/actions";

import ListMatchs from "./ListMatchs";

class Matchs extends Component {
  static propTypes = {
    getMatchs: PropTypes.func.isRequired,
    idCompetition: PropTypes.string,
    matchs: PropTypes.array.isRequired
  };

  componentDidUpdate(prevProps) {
    if (this.props.idCompetition !== prevProps.idCompetition) {
      if (this.props.idCompetition) {
        this.props.getMatchs(this.props.idCompetition);
      }
    }
  }

  render() {
    const { matchs } = this.props;
    return (
      <Fragment>
        <ListMatchs matchs={matchs} />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  matchs: state.userMatchs.matchs
});

export default connect(
  mapStateToProps,
  { getMatchs }
)(Matchs);
