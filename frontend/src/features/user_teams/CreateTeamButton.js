import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { createNewTeam } from "./actions";

class CreateTeamButton extends Component {
  static propTypes = {
    createNewTeam: PropTypes.func.isRequired
  };

  onClick = e => {
    e.preventDefault();
    const { id_competition } = this.props;
    this.props.createNewTeam(id_competition);
  };

  render() {
    return (
      <button
        className="button is-primary is-fullwidth is-large"
        onClick={this.onClick}
      >
        Add Team
      </button>
    );
  }
}

export default connect(
  null,
  { createNewTeam }
)(CreateTeamButton);
