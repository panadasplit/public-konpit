import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { createMatchs } from "./actions";

class CreateMatchsButton extends Component {
  static propTypes = {
    createMatchs: PropTypes.func.isRequired,
    id_competition: PropTypes.string.isRequired
  };

  onClick = e => {
    e.preventDefault();
    this.props.createMatchs(this.props.id_competition);
  };

  render() {
    return (
      <button
        className="button is-primary is-fullwidth is-large"
        onClick={this.onClick}
      >
        Generate Matchs
      </button>
    );
  }
}

const mapStateToProps = state => ({
  id_competition: state.userCompetitions.competition.id
});


export default connect(
  mapStateToProps,
  { createMatchs }
)(CreateMatchsButton);
