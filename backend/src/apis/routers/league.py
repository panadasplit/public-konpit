""" League routers """

from typing import List, Generator

from fastapi import APIRouter, Depends

from starlette.exceptions import HTTPException

from src.core.authentication import get_current_user_authorizer
from src.core.validators import is_competition_owner, is_league_in_competition

from src.models.user import User
from src.models.competition import CompetitionKey
from src.models.league import LeagueKey, LeagueOut
from src.models.team import Team
from src.models.match import MatchKey, MatchIn, MatchInDB, MatchOut, MatchInDBCreate, Match

from src.crud.team import get_competition_teams
from src.crud.league import (get_competition_leagues_keys, create_league,
                             get_league, delete_league_matchs,
                             get_league_matchs)
from src.crud.match import add_league_match, update_league_match

router = APIRouter()


@router.post("/{id_competition}", status_code=201)
def create_matchs(id_competition: str,
                  user: User = Depends(get_current_user_authorizer())):
    """ create and return a list of matchs """

    # validate permissions
    is_competition_owner(id_competition, user)

    competition_key = CompetitionKey(id=id_competition)

    leagues_keys = get_competition_leagues_keys(competition_key)
    if leagues_keys:
        # delete previous matchs
        for league_key in leagues_keys:
            delete_league_matchs(league_key)
        league = leagues_keys[0]
    else:
        # if not league yet, create
        league = LeagueKey(**create_league(competition_key).dict())

    # generate matchs
    teams = get_competition_teams(competition_key)
    matchs_out = []
    for match in generate_matchs(teams):
        new_match = add_league_match(league, match)
        matchs_out.append(MatchOut(**new_match.dict()))

    return matchs_out


@router.get("/{id_competition}", status_code=201)
def get_matchs(id_competition: str):
    """ create and return a list of matchs """

    competition_key = CompetitionKey(id=id_competition)

    leagues_keys = get_competition_leagues_keys(competition_key)
    if not leagues_keys:
        return []

    # generate matchs
    matchs_out = [
        MatchOut(**match.dict())
        for match in get_league_matchs(leagues_keys[0])
    ]

    return matchs_out


@router.get("/{id_competition}/{id_league}", status_code=200)
def retrieve_league(id_competition: str, id_league: str):
    """ return league """

    # TODO Check permission in future version id a competition can be private

    league = get_league(LeagueKey(id=id_league, id_competition=id_competition))

    return LeagueOut(**league.dict())


@router.patch("/{id_competition}/{id_match}", status_code=201)
def patch_match(id_competition: str,
                id_match: str,
                match: MatchIn,
                user: User = Depends(get_current_user_authorizer())):
    """ modify a league match """

    is_competition_owner(id_competition, user)

    competition = CompetitionKey(id=id_competition)

    leagues_keys = get_competition_leagues_keys(competition)
    if not leagues_keys:
        raise HTTPException(status_code=404, detail="Object not found")
    league_key = leagues_keys[0]

    updated_match = update_league_match(
        league_key, MatchKey(id=id_match, id_league=league_key.id),
        MatchInDB(**match.dict()))

    return MatchOut(**updated_match.dict())


def generate_matchs(teams: List[Team]
                    ) -> Generator[MatchInDBCreate, None, None]:
    """ generate matchs """

    nb_teams = len(teams)
    matchs = []

    if nb_teams > 1:
        home_teams = teams[:nb_teams // 2]
        away_teams = teams[nb_teams // 2:]
        if nb_teams % 2:
            home_teams.append(None)
            nb_teams += 1
        order = 1
        for day_num in range(nb_teams - 1):
            for id_team in range(nb_teams // 2):
                if None not in (home_teams[id_team], away_teams[id_team]):
                    yield MatchInDBCreate(home_team=home_teams[id_team],
                                          away_team=away_teams[id_team],
                                          order=order)
                    order += 1
            # swap
            if day_num < nb_teams - 2:
                swaped_home_team = home_teams.pop(1)
                swaped_away_team = away_teams.pop()
                home_teams.append(swaped_away_team)
                away_teams.insert(0, swaped_home_team)

    return matchs
