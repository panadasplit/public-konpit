import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getTeams } from "./actions";

import ListUserTeams from "./ListUserTeams";
import CreateTeamButton from "./CreateTeamButton";

class EditTeams extends Component {
  static propTypes = {
    getTeams: PropTypes.func.isRequired,
    teams: PropTypes.array.isRequired,
    id_competition: PropTypes.string.isRequired
  };

  componentDidMount() {
    this.props.getTeams(this.props.id_competition);
  }

  render() {
    const { teams, id_competition } = this.props;
    return (
      <Fragment>
        <CreateTeamButton id_competition={id_competition} />
        <div className="section">
          <ListUserTeams teams={teams} id_competition={id_competition} />
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  teams: state.userTeams.teams,
  id_competition: state.userCompetitions.competition.id
});

export default connect(
  mapStateToProps,
  { getTeams }
)(EditTeams);
