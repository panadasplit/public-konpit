import React, { Component } from "react";

import { Redirect } from "react-router-dom";

import { connect } from "react-redux";

import PropTypes from "prop-types";

import { login } from "./actions";

export class LoginForm extends Component {
  state = {
    email: "",
    password: ""
  };

  static propTypes = {
    login: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    isLoading: PropTypes.bool
  };

  onSubmit = e => {
    e.preventDefault();
    this.props.login(this.state.email, this.state.password);
  };

  onChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to="/" />;
    }
    const { email, password } = this.state;
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <div className="field">
            <p className="control">
              <input
                name="email"
                value={email}
                onChange={this.onChange}
                className="input is-medium"
                type="email"
                placeholder="Email"
              />
            </p>
          </div>
          <div className="field">
            <p className="control">
              <input
                name="password"
                value={password}
                onChange={this.onChange}
                className="input is-medium"
                type="password"
                placeholder="Password"
              />
            </p>
          </div>

          <button
            className="button is-primary is-medium is-fullwidth"
            type="submit"
          >
            Sign In
          </button>
        </form>

        {/* <Link to="/reset-password/">Mot de passe oublié ?</Link> */}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  isAuthenticated: !state.firebase.auth.isEmpty,
  isLoading: state.auth.isLoading
});

export default connect(
  mapStateToProps,
  { login }
)(LoginForm);
