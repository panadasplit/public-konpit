import React, { Component } from "react";

import { connect } from "react-redux";

import { logout } from "./actions";

export class LogoutButton extends Component {
  onClick = e => {
    e.preventDefault();
    this.props.logout();
  };

  render() {
    return (
      <button className="button is-primary" onClick={this.onClick}>
        <strong>Logout</strong>
      </button>
    );
  }
}

export default connect(
  null,
  { logout }
)(LogoutButton);
