""" User crud methods """

from google.cloud.firestore_v1.document import DocumentReference

from src.core.database import DB


def get_user_reference(id_user: str) -> DocumentReference:
    """ return user reference """

    return DB().client.collection("users").document(id_user)
